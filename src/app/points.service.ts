import { Injectable } from '@angular/core';

@Injectable()
export class PointsService {
  points: any;
  issues: any;
  icons: any;    
  constructor() {
  this.icons = {
        "Server Status": {isImg : false, src: 'fa-server'},  
        "Server Room AC": {isImg : true, src: 'assets/img/icons/breeze_icon.svg'},  
        "Air Quality": {isImg : false, src: 'fa-cloud'},  
        "Office Temperature": {isImg : false, src: 'fa-fire'},  
        "Vehicle Status":{isImg : false, src: 'fa-bus'},  
        "Printers": {isImg : true, src: 'assets/img/icons/print_icon.svg'},  
        "Lights": {isImg : false, src: 'fa-lightbulb-o'},  
        "Rat Traps": {isImg : true, src: 'assets/img/icons/trap_icon.svg'}, 
        "Secure Room Access": {isImg : false, src: 'fa-lock'}, 
        "Fire Supression System": {isImg : false, src: 'fa-fire-extinguisher'}, 
        "Wi-Fi Network": {isImg : false, src: 'fa-wifi'}, 
        "Fuse Board": {isImg : false, src: 'fa-bolt'}
  }  
    this.points = 
        {
            "Server Status" : [
                {"Desc" : "Low Disk space", "Points" : 10},
                {"Desc" : "Malfunction", "Points" : 20},
                {"Desc" : "Offline", "Points" : 15},
                {"Desc" : "No response", "Points" : 20},
                {"Desc" : "Low Power", "Points" : 10},
                {"Desc" : "Power Supply Failing", "Points" : 18},
                {"Desc" : "Power Supply Failed", "Points" : 20},
                {"Desc" : "Antivirus Auto Recovery", "Points" : 5},
                {"Desc" : "Antivirus Can't Fix Issue", "Points" : 18},
                {"Desc" : "High CPU Utilisation", "Points" : 8},
                {"Desc" : "Temperature Warning", "Points" : 8},
                {"Desc" : "Liquid Detected", "Points" : 20},
                {"Desc" : "Fire Detected", "Points" : 20},
                {"Desc" : "Critical Application Hang", "Points" : 19},
                {"Desc" : "Backup Failure", "Points" : 10},
                {"Desc" : "High Memory Utilisation", "Points" : 10},
                {"Desc" : "Storage Failure", "Points" : 20},
                {"Desc" : "Crash Recovered OK", "Points" : 5},
                {"Desc" : "Logs Recycled Automatically", "Points" : 1},
                {"Desc" : "Server Door Opened", "Points" : 6},
                {"Desc" : "Disk Failure", "Points" : 20},
                {"Desc" : "Scheduled Updates Completed", "Points" : 1},
                {"Desc" : "High Network Traffic", "Points" : 10},
                {"Desc" : "Unusual Network Traffic", "Points" : 18},
                {"Desc" : "Intrusion Blocked", "Points" : 5},
                {"Desc" : "Keyboard Disconnected", "Points" : 5},
                {"Desc" : "Power Surge Recovered", "Points" : 2},
                {"Desc" : "Power Surge Damage", "Points" : 15}
            ],
            "Server Room AC" : [
                {"Desc" : "High", "Points" : 20},
                {"Desc" : "Normal", "Points" : 0},
                {"Desc" : "Self Test Succeeded", "Points" : 0},
                {"Desc" : "Defrosting Automatically", "Points" : 0},
                {"Desc" : "Excessive Ice", "Points" : 10},
                {"Desc" : "Primary Fan Failure", "Points" : 20},
                {"Desc" : "No Response", "Points" : 10},
            ],
            "Air Quality" : [
                {"Desc" : "High Gas Levels", "Points" : 15},
                {"Desc" : "High CO2 Levels", "Points" : 15},
                {"Desc" : "Normal", "Points" : 0},
                {"Desc" : "No Response", "Points" : 10}
            ],
            "Office Temperature" : [
                {"Desc" : "Low", "Points" : 5},
                {"Desc" : "Normal", "Points" : 0},
                {"Desc" : "High", "Points" : 5},
                {"Desc" : "No reponse", "Points" : 10}
            ],
            "Vehicle Status" : [
                {"Desc" : "Damaged", "Points" : 5},
                {"Desc" : "Broken", "Points" : 10},
                {"Desc" : "Parked and Off", "Points" : 0},
                {"Desc" : "Parked and Running", "Points" : 15},
                {"Desc" : "No response", "Points" : 10}
            ],
            "Printers" : [
                {"Desc" : "Low Ink", "Points" : 5},
                {"Desc" : "Bad Connection", "Points" : 5},
                {"Desc" : "Offline", "Points" : 5},
                {"Desc" : "Malfunction", "Points" : 10},
                {"Desc" : "Driver Updates Required", "Points" : 8},
                {"Desc" : "Optimal Operation", "Points" : 0},
                {"Desc" : "Paper Jam", "Points" : 10},
                {"Desc" : "Paper Draw Open", "Points" : 5},
                {"Desc" : "Scanning Completed OK", "Points" : 0},
                {"Desc" : "Automatic Reboot Succeeded", "Points" : 0},
                {"Desc" : "Network Lost", "Points" : 10},
                {"Desc" : "Printing", "Points" : 10},
                {"Desc" : "Scanning", "Points" : 10},
                {"Desc" : "Power Surge Recovered", "Points" : 0}
            ],
            "Lights" : [
                {"Desc" : "On", "Points" : 0},
                {"Desc" : "Off", "Points" : 0},
                {"Desc" : "Damaged", "Points" : 5},
                {"Desc" : "Bulb Needs Replacing", "Points" : 5},
                {"Desc" : "No Response", "Points" : 2},
                {"Desc" : "High Temperature", "Points" : 5}
            ],
            "Rat Traps" : [
                {"Desc" : "Set", "Points" : 0},
                {"Desc" : "Poor Signal", "Points" : 5},
                {"Desc" : "Triggered", "Points" : 5},
                {"Desc" : "No response", "Points" : 10}
            ],
            "Secure Room Access" : [
                {"Desc" : "Locked", "Points" : 0},
                {"Desc" : "Unlocked", "Points" : 10},
                {"Desc" : "Malfunction", "Points" : 15},
                {"Desc" : "Tampered With", "Points" : 20},
                {"Desc" : "Door Forced Open", "Points" : 20},
                {"Desc" : "Door Opened with Key Card", "Points" : 0},
                {"Desc" : "Door Opened with PIN Code", "Points" : 0},
                {"Desc" : "Door Opened with Finger Print", "Points" : 0},
                {"Desc" : "Door Opened with Biometrics", "Points" : 0},
                {"Desc" : "Door Closed", "Points" : 0},
                {"Desc" : "Door Ajar", "Points" : 18},
                {"Desc" : "Door Open For 10 Minutes", "Points" : 18},
            ],
            "Fire Supression System" : [
                {"Desc" : "Test Succeeded", "Points" : 0},
                {"Desc" : "Test Failed", "Points" : 18},
                {"Desc" : "Fire Detected", "Points" : 20},
                {"Desc" : "Monitoring", "Points" : 0},
                {"Desc" : "Supression Pressure Low", "Points" : 10},
                {"Desc" : "Supression Pressure High", "Points" : 10},
                {"Desc" : "Operational", "Points" : 0},
                {"Desc" : "Operational", "Points" : 0},
                {"Desc" : "Tamper Alarm", "Points" : 10},
            ],
            "Wi-Fi Network" : [
                {"Desc" : "Operational", "Points" : 0},
                {"Desc" : "Automatic Reboot Succeeded", "Points" : 0},
                {"Desc" : "Average Network traffic", "Points" : 0},
                {"Desc" : "Above Average Network traffic", "Points" : 5},
                {"Desc" : "High Network traffic", "Points" : 9},
                {"Desc" : "Maximum Network traffic", "Points" : 19},
                {"Desc" : "Network Intrusion Blocked", "Points" : 2},
                {"Desc" : "Network Intrusion Detected", "Points" : 20},
                {"Desc" : "High Latency", "Points" : 5},
                {"Desc" : "No Internet Available", "Points" : 10},
                {"Desc" : "New Network Address Assigned Automatically", "Points" : 0},
                {"Desc" : "Network Address Conflict", "Points" : 3},
                {"Desc" : "Communication Restored", "Points" : 0},
                {"Desc" : "Primary Network Failing", "Points" : 18},
                {"Desc" : "Primary Network Failed", "Points" : 20},
            ],
            "Fuse Board" : [
                {"Desc" : "Operational", "Points" : 0},
                {"Desc" : "Fuse Reset Automatically", "Points" : 0},
                {"Desc" : "Fuse For Basement Tripped", "Points" : 5},
                {"Desc" : "Fuse For Refrigeration Blown", "Points" : 20},
                {"Desc" : "Fuse 14 needs replacing soon", "Points" : 5},
                {"Desc" : "Surge Recovered Succeeded", "Points" : 0},
                {"Desc" : "Surge Damage Detected", "Points" : 15},
                {"Desc" : "Temperature Normal", "Points" : 0}
            ]
        } 
    this.constructData();
  }
  constructData(){
    this.issues = [];  
    for (var key in this.points) {
      if (this.points.hasOwnProperty(key)) {
        this.issues.push(key);  
      }
    }
  }
  getIssues(){
    var issues = [];
    var issues_array = [];  
    while(issues.length < 3){
        var item = this.issues[Math.floor(Math.random()*this.issues.length)];
        if (issues.indexOf(item) == -1){
            issues.push(item);
        }
    }
    for (var i = 0; i < issues.length; i++){
        var iss = issues[i];
        var specific_issue = this.points[iss];
        var different = false;
        var spec_issue;
        while (!different){
            spec_issue = specific_issue[Math.floor(Math.random()*specific_issue.length)];
            if (issues_array.length == 0){
                different = true;
            }
            else if (issues_array.length == 1){
                if (spec_issue.Points != issues_array[0].spec_issue.Points){
                    different = true;
                }
            }
            else {
                if (spec_issue.Points != issues_array[0].spec_issue.Points && spec_issue.Points != issues_array[1].spec_issue.Points){
                    different = true;
                }       
            }
        }
        var issues_object = {
            name: iss,
            spec_issue: spec_issue,
            icon: this.icons[iss]
        };
        issues_array.push(issues_object);
    }     
    return issues_array;    
  }    
    

}

import { Component, OnInit } from '@angular/core';
import { ScoreService } from '../score.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {
scores: any;
constructor(private scoreService: ScoreService) { }

  ngOnInit() {
    this.scoreService.getScore().subscribe(
        (scores) => {
            this.scores = scores;
        },
        (err)=>{
            console.log(err);
        }    
    )
  }
  
}

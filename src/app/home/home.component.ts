import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router' 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public name: string;
  public twitter_handle: string;
  constructor
  (
    public user: UserService,
    private router: Router
  ) 
  {
      
  }

  ngOnInit() {
  }
  
  login(e){
    e.preventDefault();  
    this.user.setName(this.name);
    this.user.setTwitterHandle(this.twitter_handle);
    console.log(this.user.getName());
      this.router.navigate(['/instructions']);
  }    

}

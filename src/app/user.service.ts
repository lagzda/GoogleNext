import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  public name: string;
  public twitter_handle: string;    
  constructor() { 
  }
  getName(){
    return this.name;
  }
  getTwitterHandle(){
    return this.twitter_handle;
  }
  setName(name){
    this.name = name;
  }
  setTwitterHandle(twitter_handle){
    this.twitter_handle = twitter_handle;
  }       
}

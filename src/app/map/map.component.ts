import { Component, OnInit } from '@angular/core';
import { LightService } from '../light.service';
import { PointsService } from '../points.service';
import { ScoreService } from '../score.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import * as io from "socket.io-client";



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  selectedClass: number;
  locations: Array<any>;
  styles: Array<any>;
  socket: any;
  socketHost: string;
  zoom : number;
  statusClass: string;
  issues: any;
  timer: number;
  stroke: any;
  round: number;
  miniRound: number;    
  points: number;
  closeResult: string;
  interval: any; 
  disableActions: boolean;
  showModal: boolean;
  won: boolean;
  showMap: boolean;
  mStatus: number;
  mStatusClass: string;
  latlong: any;
  selection: any;
  best: any;    
  constructor
    (
        private light: LightService,
        private pointsService: PointsService,
        private scoreService: ScoreService,
        private userService: UserService,
        private router: Router
    ) 
    {
    this.locations = [
        {lat: 51.495210, lng: -0.143898},
        {lat: 40.712784, lng: -74.005941},
        {lat: -33.868820, lng: 151.209296}
    ]
    this.mStatus = 3;
    this.mStatusClass = 'mstatus'+this.mStatus.toString(); 
    this.showModal = false;  
    this.points = 0;  
    this.round = 1;
    this.miniRound = 1;  
    this.stallTime();  
    this.issues = this.pointsService.getIssues();
    console.log(this.issues);  
    this.zoom = 6;   
    this.socketHost = "http://104.155.88.243:8080/";
    this.socket = io(this.socketHost);
      
  
    var that = this;  
    this.socket.on("Flic", (button) => {
      that.doAction(button);
    });  
    this.styles = 
[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ebe3cd"
      }
    ]
  },
  {
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#523735"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f1e6"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#c9b2a6"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#dcd2be"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#ae9e90"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#93817c"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a5b076"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#447530"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f1e6"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#fdfcf8"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f8c967"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#e9bc62"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e98d58"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#db8555"
      }
    ]
  },
  {
    "featureType": "road.local",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#806b63"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8f7d77"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#ebe3cd"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#b9d3c2"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#92998d"
      }
    ]
  }
];  
        
  }

  ngOnInit() {
  }
  select(nr){
    this.selectedClass = nr;
  }
  doAction(action){
    if (!this.disableActions){
        this.miniRound += 1;  
        var selection;
        if (action == "green"){
            this.selection = this.issues[0];
        }
        else if (action == "black"){
            this.selection = this.issues[1];
        }
        else{
            this.selection = this.issues[2];
        }
        this.checkBest(this.selection);
        var that = this;
        setTimeout(function(){
            that.issues = that.pointsService.getIssues(); 
        }, 1000);  
        var timeout;
        if (this.miniRound >= 6){
            if(this.round == 3){
                this.won = true;
                this.sendScore({
                    name: this.userService.getName(),
                    score: this.points
                });
                this.showModal = true;
                clearInterval(this.interval);
                return;
            }
            this.round += 1;
            this.miniRound = 1;
            this.statusClass = 'doneRound';
            this.stallTime();
            timeout = 5000;
        }
        else{
            this.statusClass = 'done';
            timeout = 2000;
        }
        setTimeout(function(){
            that.statusClass = '';
        }, timeout);
    }   
  }
  
  getLight(){
    this.light.getLight().subscribe(
      (light) =>{
        console.log(light);
      },
      (err) =>{
        console.log(err);
      }    
    )
  }
  putLight(status){
    this.light.putLight(status).subscribe(
      (light) =>{
        console.log(light);
      },
      (err) =>{
        console.log(err);
      }    
    )
  }
    
  sendScore(s){
    this.scoreService.sendScore(s).subscribe(
      (score) =>{
        console.log(score);
      },
      (err) =>{
        console.log(err);
      }    
    )
  }    
    
  stallTime(){
    var that = this;
    this.stroke = 0;
    if(this.round == 1){
        this.timer = 60;
    }
    else if(this.round == 2){
        this.timer = 45;
    }
    else{
        this.timer = 30;
    }  
    this.disableActions = true; 
    this.showMap = false;
    this.latlong = this.locations[Math.floor(Math.random()*this.locations.length)]; 
    if(this.interval){
        clearInterval(this.interval);  
    }  
    var timeout = setTimeout(function(){
        that.disableActions = false;
        that.showMap = true;
        that.runTimer();
    }, 17000);
  }
  runTimer(){  
    var time;  
    if(this.round == 1){
        time = 60;
    }
    else if(this.round == 2){
        time = 45;
    }
    else{
        time = 30;
    }
    
    var initialOffset = 440;
    var i = 1;
    var that = this;
    this.interval = setInterval(function() {
		that.timer = time - i;
        if (i == time) {
            that.disableActions = true;
            that.won = false;
            that.showModal = true;
            clearInterval(that.interval);
            that.sendScore({
                name: that.userService.getName(),
                score: that.points
            });
            return;
        }
        that.stroke = 0+((i+1)*(initialOffset/time));
        i++;  
    }, 1000);  
  }
    goToLB(){
      this.router.navigate(['/leaderboard']);
    }
     checkBest(selection){
        this.best = 2;
        var action;
        var sel;
        for (var i = 0; i < this.issues.length; i++){
            if(selection.spec_issue.Points < this.issues[i].spec_issue.Points){
                this.best -= 1;
            }
            else if(selection.spec_issue.Points == this.issues[i].spec_issue.Points){
                this.points += selection.spec_issue.Points;
            }
        }
        if (this.best == 2){
            action = "green";
            if (this.mStatus < 5){
                this.mStatus += 1;
            }
            
        }
        else if (this.best == 1){
            action = "yellow";
        }
        else{
            action = "black";
            if (this.mStatus > 1){
                this.mStatus -= 1;
            }
        }
    this.mStatusClass = 'mstatus' + this.mStatus.toString();
        var that = this;
        var iterations = 0; 
        var lightInterval = setInterval(function(){
        if(iterations % 2 == 0){
            that.putLight(action);
        } else{
            that.putLight("default");
        }
        if(iterations == 3){
            clearInterval(lightInterval);
        }
        iterations++;    
        }, 600);         
    }
}
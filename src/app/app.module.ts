import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from 'angular2-google-maps/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MapComponent } from './map/map.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { routing, appRoutingProviders } from './app.routing';
import { UserService } from './user.service';
import { LightService } from './light.service';
import { PointsService } from './points.service';
import { ScoreService } from './score.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MapComponent,
    LeaderboardComponent,
    InstructionsComponent  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule,
    routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB7j8hEJsgc7zYUDHagoPbJBt6ypW5nlBA'
    })  
  ],
  providers: [
    appRoutingProviders,
    UserService,
    LightService,
    PointsService,  
    ScoreService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

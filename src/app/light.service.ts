import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
// Statics
import 'rxjs/add/observable/throw';

// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LightService {
  private lightUrl = 'http://192.168.0.101/api/cLB-QB8YuQGRj0TsGl0vqoOTAB9851n1Kb7l4jju/lights/1';
  constructor(private http: Http) {
     
  }
  getLight (): Observable<any[]> {
      return this.http.get(this.lightUrl)
                      .map(this.extractData)
                      .catch(this.handleError);
  }
  putLight (status: string): Observable<any> {
    var body = {};  
    if (status == "green"){
        body = {"on":true, 
                "sat":150, 
                "bri":154, 
                "hue":10000,
                "xy": [0.2682,0.6632]
               }; 
    }else if (status == "black"){ 
        body = {"on":true, 
                "sat":150, 
                "bri":154, 
                "hue":10000,
                "xy": [0.6531,0.2834]
               };
    }
    else if (status == "yellow"){
        body = {"on":true, 
                    "sat":150, 
                    "bri":154, 
                    "hue":10000,
                    "xy": [0.5951,0.3872]
                   };

    }
    else{
        body = {
            "on":false
        }
    }  
    body = JSON.stringify(body);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.put(this.lightUrl+'/state', body, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }
   
  private extractData(res: Response) {
    let body = res.json();
    console.log(body);  
    return body || { };
  }
  private handleError (error: any) {
  // In a real world app, we might use a remote logging infrastructure
  // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }   
}

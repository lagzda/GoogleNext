import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { MapComponent } from './map/map.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { InstructionsComponent } from './instructions/instructions.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'map',
    component: MapComponent,
    data: {
      title: 'Heroes List'
    }
  },
  {
    path: 'instructions',
    component: InstructionsComponent,
    data: {
      title: 'Heroes List'
    }
  },
  {
    path: 'leaderboard',
    component: LeaderboardComponent,
    data: {
      title: 'Heroes List'
    }
  },    
  { path: '', component: HomeComponent },
  { path: '**', component: HomeComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

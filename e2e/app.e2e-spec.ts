import { GoogleNextPage } from './app.po';

describe('google-next App', function() {
  let page: GoogleNextPage;

  beforeEach(() => {
    page = new GoogleNextPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
